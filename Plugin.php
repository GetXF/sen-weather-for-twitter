<?php
/**
 * SenWeather For Twitter是typecho的天气插件<br/>适用于油油的Twitter主题<br/>建议、Bug反馈：<a target="_blank" href="https://gitee.com/GetXF/sen-weather-for-twitter/issues">点击反馈</a>
 *
 * @package SenWeather
 * @author XiaoFans
 * @version 1.2.9
 * @link https://gitee.com/GetXF/sen-weather-for-twitter/issues
 */
class SenWeather_Plugin implements Typecho_Plugin_Interface {

    public static function activate(){
        Typecho_Plugin::factory('Widget_Archive')->header = array('SenWeather_Plugin', 'header');
        Typecho_Plugin::factory('Widget_Archive')->footer = array('SenWeather_Plugin', 'footer');
    }

    public static function deactivate(){	
	}

	public static function config(Typecho_Widget_Helper_Form $form) {
		$token = new Typecho_Widget_Helper_Form_Element_Text(
			'token',NULL,'68ddc93c-3f29-4935-ac03-d7eafe898099',
			_t('心知天气Token：'),
			_t('必填项，此Token为公共Token，使用个人Token请输入。<br>注册网址:https://www.seniverse.com/dashboard')
		);
        $form->addInput($token);
        $flavor = new Typecho_Widget_Helper_Form_Element_Radio('flavor',
        array(
            'slim' => '固定',
            'bubble' => '浮动'
        ),'bubble', _t('插件显示方式'), _t('
            插件显示方式，浮动即在整个页面左上角位置显示，固定则在搜索框后面显示。   
        '));
        $form->addInput($flavor);	
		$language = new Typecho_Widget_Helper_Form_Element_Select("language", 
			array(
			'zh-Hans' => '简体中文',
			'zh-Hant' => '繁体中文',
			'en' => '英文',
			'ja' => '日语',
			'de' => '德语',
			'fr' => '法语',
			'es' => '西班牙语',
			'pt' => '葡萄牙语',
			'hi' => '印地语（印度官方语言之一）',
			'id' => '印度尼西亚语',
			'ru' => '俄语',
			'th' => '泰语',
			'ar' => '阿拉伯语',
			'auto' => '自适应'
        ), 'zh-Hans', "语言", "显示语言选择");
        $form->addInput($language);
		$geolocation = new Typecho_Widget_Helper_Form_Element_Radio('geolocation',
            array(
                'true' => '启用',
                'false' => '关闭'
            ),'true', _t('自动定位'), _t('
                启用则对当前所在位置使用网络定位或者使用IP定位，失效则默认显示北京。
            ')
        );
        $form->addInput($geolocation);
		$location = new Typecho_Widget_Helper_Form_Element_Text(
			'location',NULL,'WX4FBXXFKE4F',
			_t('城市'),
			_t('此处支持<code>城市ID</code>、<code>城市中文名</code>、<code>省市组合名称</code>、<code>城市拼音/英文名</code>、<code>经纬度</code>、<code>IP地址</code><br><p style="color:red">说明：（如自动定位失效请手动填写，国外城市需要使用收费token，公用token暂不支持，如果失效请使用城市ID，ID列表在下方）<br>1. 城市ID，推荐使用，例如：WX4FBXXFKE4F（北京）<br>2. 城市中文名，例如：北京<br>3. 省市名称组合，例如：北京朝阳<br>4. 城市拼音/英文名,拼音相同城市，在之前加省份和空格例如：beijing chaoyang<br>5. 经纬度,格式是 纬度:经度，英文冒号分隔,例如：39.93:116.40<br>6.IP地址，某些 IP 地址可能无法定位到城市，例如：210.31.73.4</p>')
		);
        $form->addInput($location);
		$unit = new Typecho_Widget_Helper_Form_Element_Radio('unit',
            array(
                'c' => '℃ 摄氏度',
                'f' => '℉ 华氏度'
            ),'c', _t('默认单位'), _t('
                单位不同会导致温度、风速、能见度和气压的单位会发生变化，切换为华氏度风速将以mph（英里每小时）计量，能见度将会以mi（英里）计量，气压将会以[in Hg](英寸汞柱)计量。详情阅：<a href="https://docs.seniverse.com/api/start/unit.html">点击查看</a>     
            ')
        );
        $form->addInput($unit);
/*新功能（待开发）
        $style - new Typecho_Widget_Helper_Form_Element_Radio('style',
            array(
                'ys' => '原生样式',
                'zs' => '专属样式',
            ),'zs',_t('
                固定模式样式设置，原生样式将显示插件固定模式的本来样式，专属样式将显示Twitter主题专属样式。
                ')
        );
        $form->addInput($style);
**/
		$theme = new Typecho_Widget_Helper_Form_Element_Radio('theme',
            array(
                'auto' => '随天气变化',
                'light' => '白色',
				'dark' => '黑色'
            ),'auto', _t('插件主题色'), _t('
                选择插件显示主题色。     
            ')
        );
        $form->addInput($theme);
        $color = new Typecho_Widget_Helper_Form_Element_Radio('color',
        array(
            'black' => '黑色',
            'white' => '白色'
        ),'black', _t('插件文本颜色'), _t('
            固定模式下，插件文本颜色与导航条背景色冲突后请切换颜色。     
        '));
        $form->addInput($color);
		$btf = new Typecho_Widget_Helper_Form_Element_Radio('btf',
            array(
                'none' => '无',
                'saibo' => '赛博朋克',
                'adk' => 'ADK阿卡丽',
                'nitai' =>'简洁拟态'
            ),'none', _t('美化主题'), _t('
                启用美化主题，改变后插件主题色默认失效。 <div style="color:red;">（推荐使用美化主题时使用浮动模式，相关主题启用后请到后台微调字体大小颜色等）</div>
            ')
        );
        $form->addInput($btf);
        $height = new Typecho_Widget_Helper_Form_Element_Text('height',
        NULL,'6',
        _t('上下间距微调'),
        _t('固定模式下，插件位置与导航条不居中；更换美化主题后，上下间距会发生变化，在此微调（px）。')
        );
        $form->addInput($height);
        $fuweio = new Typecho_Widget_Helper_Form_Element_Text('fuweio',
        NULL,'none',
            _t('浮动模式下的位置调整left'),
            _t('定位插件元素左外边距边界与其包含块左边界之间的偏移量，数字越大越往右移动（填数字请加单位px）,支持auto，inherit，initial，revert，unset。<br><div style="color:red;">如不使用请输入none</div>')
        );
        $form->addInput($fuweio);
        $fuweit = new Typecho_Widget_Helper_Form_Element_Text('fuweit',
        NULL,'none',
            _t('浮动模式下的位置调整right'),
            _t('定位插件元素右外边距边界与其包含块右边界之间的偏移量（填数字请加单位px）,支持auto，inherit，initial，revert，unset。<br><div style="color:red;">不建议调整，如不使用请输入none</div>')
        );
        $form->addInput($fuweit);
        $fuweis = new Typecho_Widget_Helper_Form_Element_Text('fuweis',
        NULL,'none',
            _t('浮动模式下的位置调整top'),
            _t('定位插件元素的上外边距边界与其包含块上边界之间的偏移量，数字越大越往下移动（填数字请加单位px）,支持auto，inherit，initial，revert，unset。<br><div style="color:red;">如不使用请输入none</div>')
        );
        $form->addInput($fuweis);
        $fuweif = new Typecho_Widget_Helper_Form_Element_Text('fuweif',
        NULL,'none',
            _t('浮动模式下的位置调整bottom'),
            _t('定位插件元素下外边距边界与其包含块下边界之间的偏移量（填数字请加单位px）,支持auto，inherit，initial，revert，unset。<br><div style="color:red;">不建议调整，如不使用请输入none</div>')
        );
        $form->addInput($fuweif);
        $csco = new Typecho_Widget_Helper_Form_Element_Text('csco',
        NULL,'white',
        _t('城市颜色微调'),
        _t('详情页左上角城市颜色微调；更换美化主题后，颜色会发生变化，导致叠色等情况，在此微调（请使用十六进制颜色代码或颜色单词）。')
        );
        $form->addInput($csco);
        $bqco = new Typecho_Widget_Helper_Form_Element_Text('bqco',
        NULL,'white',
        _t('版权颜色微调'),
        _t('详情页下方版权信息颜色微调；更换美化主题后，颜色会发生变化，导致叠色等情况，在此微调（请使用十六进制颜色代码或颜色单词）。')
        );
        $form->addInput($bqco);
		$tips = new Typecho_Widget_Helper_Form_Element_Text(
			'tips',NULL,'如遇bug和有好的建议，请点击下方按钮反馈。',
			_t('意见反馈'),
			_t('<a  href="https://gitee.com/GetXF/sen-weather-for-twitter/issues"><button type="button" class="anniu">点击反馈</button></a> &nbsp; <a  href="https://gitee.com/GetXF/sen-weather-for-twitter/"><button type="button" class="anniu">官方gitee库</button></a> &nbsp; <a  href="https://cdn.sencdn.com/download/data/thinkpage_cities.zip"><button type="button" class="anniu">城市ID下载(.xls)</button></a> &nbsp; <a  href="../usr/plugins/SenWeather/images/ID.png"><button type="button" class="anniu">城市ID查看（png）</button></a><style>.anniu{background:#3498db;background-image:-webkit-linear-gradient(top,#3498db,#2980b9);background-image:-moz-linear-gradient(top,#3498db,#2980b9);background-image:-ms-linear-gradient(top,#3498db,#2980b9);background-image:-o-linear-gradient(top,#3498db,#2980b9);background-image:linear-gradient(to bottom,#3498db,#2980b9);-webkit-border-radius:9;-moz-border-radius:9;border-radius:9px;text-shadow:1px 1px 3px#666666;-webkit-box-shadow:0px 1px 2px#666666;-moz-box-shadow:0px 1px 2px#666666;box-shadow:0px 1px 2px#666666;font-family:Georgia;color:#ffffff;font-size:19px;padding:3px 5px 4px 6px;border:solid#1f628d 2px;text-decoration:none}.anniu:hover{background:#3cb0fd;background-image:-webkit-linear-gradient(top,#3cb0fd,#3498db);background-image:-moz-linear-gradient(top,#3cb0fd,#3498db);background-image:-ms-linear-gradient(top,#3cb0fd,#3498db);background-image:-o-linear-gradient(top,#3cb0fd,#3498db);background-image:linear-gradient(to bottom,#3cb0fd,#3498db);text-decoration:none}</style>')
		);
		$form->addInput($tips);
    }

	public static function personalConfig(Typecho_Widget_Helper_Form $form){
	}

    public static function header() {
        $btf = Helper::options()->plugin('SenWeather')->btf;
        $options = Helper::options();
        $cssUrl = $options->pluginUrl;
        if($btf == 'none') {
            echo <<<HTML
<!--心知天气（未启用美化主题）-->
<link type="text/css" rel="stylesheet" href="{$cssUrl}/SenWeather/css/style.css" />
HTML;
        }
        if($btf == 'saibo') {
            echo <<<HTML
<!--心知天气-->
<link type="text/css" rel="stylesheet" href="{$cssUrl}/SenWeather/css/style.css" />
<link type="text/css" rel="stylesheet" href="{$cssUrl}/SenWeather/css/CYB.css" />
HTML;
        }
        if($btf == 'adk') {
            echo <<<HTML
<!--心知天气-->
<link type="text/css" rel="stylesheet" href="{$cssUrl}/SenWeather/css/style.css" />
<link type="text/css" rel="stylesheet" href="{$cssUrl}/SenWeather/css/ADK.css" />
HTML;
        }
        if($btf == 'nitai') {
            echo <<<HTML
<!--心知天气-->
<link type="text/css" rel="stylesheet" href="{$cssUrl}/SenWeather/css/style.css" />
<link type="text/css" rel="stylesheet" href="{$cssUrl}/SenWeather/css/nitai.css" />
HTML;
        }
    }



    public static function footer(){
	    $sensettings = Helper::options()->plugin('SenWeather');
        echo <<<HTML
<!--心知天气-->
  <style>
    p.sw-typography.sc-bwzfXH.jCzPjW {
	    color:$sensettings->bqco;
    }
    a.sc-jzJRlG.ggJaPA {
	    color:$sensettings->csco;
     }
    .sc-csuQGl.sw-container.sc-eqIVtm.fsIvwB {
	    margin-top:{$sensettings->height}px;
    }
    p.sw-typography.sw-ui-tile-header.sc-bwzfXH.lgjLwG {
        font-size:14px;
    }
    span.sw-typography.sw-bar-slim-temperature.sc-bwzfXH.dsrBSL {
        font-size:{$sensettings->ssize}px;
    }
    #tp-weather-widget .sw-container {
        left: $sensettings->fuweio;
        right: $sensettings->fuweit;
        top: $sensettings->fuweis;
        bottom: $sensettings->fuweif;
    }
    span.sw-typography.sw-bar-slim-location.sc-bwzfXH.dsrBSL {
        display: none;
    }
    span.sw-typography.sw-bar-slim-temperature.sc-bwzfXH.dsrBSL {
        display: none;
    }
    #left {
        z-index: 0;
    }
  </style>
  <script>
var darkbtn = document.createElement("span");
darkbtn.innerHTML = '<div id="tp-weather-widget" class="navbar-form navbar-form-sm navbar-left shift" style="float:right;"></div>';
if (document.getElementById('moonlight')) {
    var darkother = document.getElementById('moonlight').parentElement;
    darkother.insertBefore(darkbtn, document.getElementById('moonlight'));
}
  </script>
  <script>
  console.log("%cSenWeather For Twitter v1.2.9 By XiaoFans | https://gitee.com/GetXF/sen-weather-for-twitter/","color:#fff;background: linear-gradient(to right , #E8CBC0, #636FA4);padding:5px;border-radius: 5px;");  </script>
  <script>
    (function(a,h,g,f,e,d,c,b){b=function(){d=h.createElement(g);c=h.getElementsByTagName(g)[0];d.src=e;d.charset="utf-8";d.async=1;c.parentNode.insertBefore(d,c)};a["SeniverseWeatherWidgetObject"]=f;a[f]||(a[f]=function(){(a[f].q=a[f].q||[]).push(arguments)});a[f].l=+new Date();if(a.attachEvent){a.attachEvent("onload",b)}else{a.addEventListener("load",b,false)}}(window,document,"script","SeniverseWeatherWidget","//cdn.sencdn.com/widget2/static/js/bundle.js?t="+parseInt((new Date().getTime() / 100000000).toString(),10)));
    window.SeniverseWeatherWidget('show', {
      flavor: "$sensettings->flavor",
      location: "$sensettings->location",
      geolocation: $sensettings->geolocation,
      language: "$sensettings->language",
      unit: "$sensettings->unit",
      theme: "$sensettings->theme",
      token: "$sensettings->token",
      hover: "enabled",
      container: "tp-weather-widget"
    })
  </script>
<!--心知天气-->
HTML;
    }
}
